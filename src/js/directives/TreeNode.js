angular
    .module('golazoApp')
    .directive('treeNode', TreeNode)
    .filter('nameFilter', NameFilter);

function TreeNode( $compile, $stateParams ) {
    return {
        restrict: 'E',
        templateUrl: "partials/directives/treenode.html",
        replace: true,
        controller: ["$scope", function($scope){
            var tnc = this;
            
            this.openNode= function(node, event){
                if(node.Childrens.length > 0)
                  event.preventDefault();
                if( !node.isSelected )
                    node.isExpanded = !node.isExpanded;
            }
            
            this.getIconClass = function(){
                var node = tnc.node;
                var icon = "asterisk";
                if( node.Childrens.length === 0 ){
                    icon = "asterisk";
                }else{
                    icon = "plus-sign"
                    if( node.isExpanded ){
                        icon = "minus-sign"
                    }
                }
                if(node.isSelected){
                    icon = "star";
                }
                return icon;
            }
        }],
        controllerAs: "tnc",
        scope: {
            node: '=',
            parent: '@'
        },
        bindToController: {
            node: '=',
            parent: '@'
        },
        link: function (scope, element, attrs) {
          //check if this node has children
           
           if (angular.isArray(scope.node.Childrens) && scope.node.Childrens.length > 0) {
              element.append('<ul class="childTree" data-ng-show="tnc.node.isExpanded"><tree-node data-ng-repeat="childNode in tnc.node.Childrens" data-node="childNode"></tree-node></ul>');
              var ul = element.find("ul");
              // only recompile the added element
              $compile(ul)(scope);
          }
        }
        
    }
}

function NameFilter(){
    return function(input) {
        return input.charAt(0) === '#' ? input.substring(1) : input;
    };
}

TreeNode.$inject = ['$compile', '$stateParams'];