function CategoriesController( categories, $rootScope, costCategoriesService){
    
    this.categories = categories;
    
    $rootScope.$on('$stateChangeStart', 
        function(event, toState, toParams, fromState, fromParams){ 
            if( toState.name === 'categories')
                costCategoriesService.setSelectedNode( -1 );
        })
    
}

CategoriesController.$inject = ["categories", "$rootScope", 'costCategoriesService'];

angular.module("golazoApp").controller("categoriesController", CategoriesController);