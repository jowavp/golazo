// service
angular
    .module('golazoApp')
    .service('costCategoriesService', costCategoriesService);

function costCategoriesService($http, $q) {
    
    this.treeNodes = null;
    
    this.getAllCostCategories = function(){
        var d = $q.defer();
        var service = this;
        $http.get('http://104.45.81.72:9011/api/project/abc/detailgroups').
          success(function(data, status, headers, config) {
            service.treeNodes = data;
            return d.resolve( service.treeNodes );
          }).
          error(function(data, status, headers, config) {
            service.treeNodes = null;
            console.log( data );
            d.reject( data );
          });
        return d.promise;
    }
    
    this.setSelectedNode = function( id ){
        // id = -1 --> reset tree
        
        //convert id to integer.
        id = parseInt( id );
        
        // search for the selected node
        angular.forEach( this.treeNodes, function(value, key){
            
            function hasOrIs(value, id){
                
                var found = false;
                angular.forEach( value.Childrens, function(value, key){
                    value.isSelected = hasOrIs( value, id );
                    if( value.isSelected ){
                        value.isExpanded = true;
                        found = true;
                    }else if( id === -1){
                        value.isExpanded = false;
                    }
                    
                    this[key] = value;
                });
                return found || value.Id === id;
            }
            
            value.isSelected = hasOrIs( value, id );
            if( value.isSelected ){
                value.isExpanded = true;
            }else if( id === -1){
                value.isExpanded = false;
            };
            this[key] = value;
        }, this.treeNodes);
    
    }
    
}

costCategoriesService.$inject = ["$http", "$q"];