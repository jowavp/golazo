angular.module('golazoApp', ['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {
  //
  $urlRouterProvider.otherwise('/categories');
  //
  // Now set up the states
  $stateProvider
    .state('categories', {
      url: '/categories',
      templateUrl: 'partials/categories.html',
      controller: 'categoriesController',
      controllerAs: "cc",
      resolve: {
            'categories': ['costCategoriesService', function(costCategoriesService){ 
          return costCategoriesService.getAllCostCategories();
      }] }
    })
    .state('categories.detail', {
      url: '/:path',
      templateUrl: 'partials/detail.html',
      onEnter: ['costCategoriesService', '$stateParams', function(costCategoriesService, $stateParams){
        costCategoriesService.setSelectedNode( $stateParams.path );
      }],
      controller: [ '$stateParams',
          function($stateParams){
              this.selectedNodeId = $stateParams.path;
          }
        ],
      controllerAs: "cdc"
    });
    
});;